<?php
    $testimonialHeading = get_theme_mod('heading_for_testimonial', 'Testimonial');
?>
<section class="home-section testimonials-layout-1" id="testimonials">
    <div class="container">
        <div class="main-title">
            <?php if ($testimonialHeading) { ?>
                <h2 class="title title-1"><?php echo esc_html($testimonialHeading); ?></h2>
            <?php } ?>
        </div>
        <div class="testimonials-holder owl-carousel owl-theme">
            <?php
            $args =[
                'posts_per_page' => '-1',
                'post_type'     => 'testimonial',
                'post_status'   => 'publish',
                'order'         => 'DESC'
            ];
            query_posts($args);
            $counter = 0;
            if (have_posts()) {
            while( have_posts()) : the_post();
                $custom = get_post_custom();
                ?>
                <div class="item">
                    <span class="fa fa-quote-left"></span>
                    <?php the_content();?>
                    <h6><?php the_title(); ?></h6>
                </div>
                <?php
                $counter++;
            endwhile;
            }else{
                esc_html_e('No data found');
            }
            wp_reset_query();
            flush();
            ?>

        </div>
    </div>
</section>

