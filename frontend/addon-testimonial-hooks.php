<?php


add_action('WEP_testimonial_addon_customize_layouts', 'WEP_selected_customize_testimonial_layout');

function WEP_selected_customize_testimonial_layout()
{

    $selecttestimonialLayout = get_theme_mod('one_page_conference_testimonial_layouts','1');

    include_once dirname(__FILE__) . '/layout/testimonial/testimonial-layout-'.$selecttestimonialLayout.'.php';

}
add_action('WEP_testimonial_addon_shortcode_layouts', 'WEP_selected_shortcode_testimonial_layout');

function WEP_selected_shortcode_testimonial_layout()
{

    $my_var = get_query_var('testimonialShortcodeLayout');
    if ($my_var > 0 AND $my_var < 5 ) {
        include_once dirname(__FILE__) . '/layout/testimonial/testimonial-layout-' . $my_var . '.php';
    } else {
        include_once dirname(__FILE__) . '/layout/testimonial/testimonial-layout-1.php';
    }

}


