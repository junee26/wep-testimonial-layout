<?php
/**
 * Plugin Name: WEP Testimonial Layout Addon
 * Description: An add-on for Event Management
 * Version: 1.2.0
 * Author:
 * Author URI:
 * License:
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
define('EVENT_TESTIMONIAL_ADDON_URL', plugins_url('', __FILE__));

if ( ! class_exists('WepTestimonialAddonPlugin')) {
    class WepTestimonialAddonPlugin{

        function testimonialLayout_addon_register(){

            add_action( 'admin_notices', array( $this, 'WEP_success_testimonialpage_admin_notice' ) );

            add_action('plugins_loaded', array( $this, 'testimonialLayout_addon_load_file' ) );

            add_action( 'customize_register', array( $this, 'wep_customize_testimonial_layout_section' ),'99');

        }

        function testimonialLayout_addon_load_file(){
            include('frontend/addon-testimonial-hooks.php');
        }

        function WEP_testimonialAddon_activation()
        {
            flush_rewrite_rules();
        }

        function WEP_testimonialAddon_deactivation()
        {
            flush_rewrite_rules();
        }

        function WEP_success_testimonialpage_admin_notice() {
             // Verify that WEP is active
             $wep_plugin = 'wp-event-partners/wpeventpartners.php'; 
             $pathpluginurl = WP_PLUGIN_DIR .'/'. $wep_plugin;
             $isinstalled = file_exists( $pathpluginurl );
             if($isinstalled){
                 if (class_exists('WepPlugin')) {
 
                     $wpcf7_path = plugin_dir_path( dirname(__FILE__) ) . 'wp-event-partners/wpeventpartners.php';
                     $wpcf7_plugin_data = get_plugin_data( $wpcf7_path, false, false);
                    
                 }
             
                 // If it's not installed and activated, throw an error
                 else {
                     $plugin = 'wp-event-partners/wpeventpartners.php';
                     $activation_url = wp_nonce_url( 'plugins.php?action=activate&amp;plugin=' . $plugin . '&amp;plugin_status=all&amp;paged=1&amp;s', 'activate-plugin_' . $plugin );
                     $message = '<p>' . __( 'WPeventpartners Testimonial Layout Addon is not working because you need to activate the WPeventpartners plugin.' ) . '</p>';
                     $message .= '<p>' . sprintf( '<a href="%s" class="button-primary">%s</a>', $activation_url, __( 'Activate WPeventpartners Now' ) ) . '</p>';
     
                     echo '<div class="error"><p>' . $message . '</p></div>';
                 }
             }else {
                 $search_url = admin_url( 'plugin-install.php?s=wpeventpartner&tab=search&type=term' );
     
                 echo '<div class="error notice is-dismissible"><p><strong>' . $this->plugin_full_name . '</strong> &#8211; ' . sprintf( esc_html__( 'This requires %s plugin to be activated to work.', 'Schedule-Pro' ), '<a href="'.esc_url( $search_url ).'">' . esc_html__('WP Event Partners','Schedule-Pro'). '</a>' ) . '</p></div>';
             
             }
        }

        function wep_customize_testimonial_layout_section($wp_customize)
        {
            $wp_customize->add_setting('one_page_conference_testimonial_layouts', array(
                'sanitize_callback' => 'one_page_conference_sanitize_choices',
                'default' => '1',
            ));

            $wp_customize->add_control(new One_Page_Conference_Radio_Image_Control($wp_customize, 'one_page_conference_testimonial_layouts', array(
                'label' => esc_html__('Select Layout For Testimonial Section', 'one-page-conference'),
                'section' => 'one_page_conference_testimonial_sections',
                'settings' => 'one_page_conference_testimonial_layouts',
                'type' => 'radio-image',
                'choices' => array(
                    '1' => WEP_EVENT_URL . '/admin/assets/images/homepage/header-layouts/header-layout-two.jpg',
                    '2' => WEP_EVENT_URL . '/admin/assets/images/homepage/header-layouts/header-layout-two.jpg',
                    '3' => WEP_EVENT_URL . '/admin/assets/images/homepage/header-layouts/header-layout-two.jpg',
                    '4' => WEP_EVENT_URL . '/admin/assets/images/homepage/header-layouts/header-layout-two.jpg',
                ),
            )));

        }

    }
    $wepTestimonialLayout = new WepTestimonialAddonPlugin();
    $wepTestimonialLayout->testimonialLayout_addon_register();

}
register_activation_hook( __FILE__, array( $wepTestimonialLayout, 'WEP_testimonialAddon_activation') );
register_deactivation_hook( __FILE__, array( $wepTestimonialLayout, 'WEP_testimonialAddon_deactivation') );